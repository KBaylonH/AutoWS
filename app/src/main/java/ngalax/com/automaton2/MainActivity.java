package ngalax.com.automaton2;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;
import cz.msebera.android.httpclient.Header;
import ngalax.com.automaton2.util.AutomatonService;
import ngalax.com.automaton2.util.ContactHelper;
import ngalax.com.automaton2.util.Rest;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private final String TAG = this.getClass().getName();
    public static JSONArray _numeros = null;
    public String _mensaje = null;

    public static boolean started = false;
    public static MainActivity instance = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        (findViewById(R.id.btnStart)).setOnClickListener(this);
        instance = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED ||
                    checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED  ||
                    checkSelfPermission(Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED  ||
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED  ||
                    checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE,Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS,Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE}, 999);
            }
        }
    }

    @Override
    public void onClick(View v){
       Log.d(TAG, "Estoy haciendo clic");
       init();
    }

    public void openWhatsapp(){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, _mensaje);
        sendIntent.setPackage("com.whatsapp");
        sendIntent.setType("text/plain");
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        sendIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(sendIntent);
    }

    public boolean isAccessibilityEnabled(){
        int accessibilityEnabled = 0;
        boolean accessibilityFound = false;

        try {
            accessibilityEnabled = Settings.Secure.getInt(this.getContentResolver(),android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            Log.d(TAG, "ACCESSIBILITY: " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            Log.d(TAG, "Error finding setting, default accessibility to not found: " + e.getMessage());
        }

        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled==1){
            Log.d(TAG, "***ACCESSIBILIY IS ENABLED***: ");


            String settingValue = Settings.Secure.getString(getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            Log.d(TAG, "Setting: " + settingValue);
            if (settingValue != null) {
                TextUtils.SimpleStringSplitter splitter = mStringColonSplitter;
                mStringColonSplitter.setString(settingValue);
                while (splitter.hasNext()) {
                    String accessabilityService = splitter.next();
                    Log.d(TAG, "Setting: " + accessabilityService);
                    if (accessabilityService.equalsIgnoreCase("ngalax.com.automaton2/ngalax.com.automaton2.util.AutomatonService")){
                        Log.d(TAG, "We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }

            Log.d(TAG, "***END***");
        }
        else{
            Log.d(TAG, "***ACCESSIBILIY IS DISABLED***");
        }
        return accessibilityFound;
    }

    private boolean checkAccessibility(){
        if(!isAccessibilityEnabled()){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Para servirle mejor, Automaton necesita permiso en la configuración de accessibilidad.\r\nEsto es sólo para própositos de automatización de Whatsapp. Sus datos y acciones seguirán siendo confidenciales y seguros.")
                    .setCancelable(false)
                    .setPositiveButton("Abrir Configuración", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //do things
                            startActivityForResult(new Intent(android.provider.Settings.ACTION_ACCESSIBILITY_SETTINGS), 0);
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            return true;
        }

        return false;
    }

    public void init(){

        started = false;

        Log.v(TAG, "checkAccessibility");
        if(!checkAccessibility()){
            Log.v(TAG, "Hay un error en checkAccessibility");
            return;
        }

        Log.v(TAG, "SOLICITUD DE DATOS ==========================");
        (new Rest()).getMessages(new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject data) {
                try {
                    Log.v(TAG, "DATOS OBTENIDOS ==========================");
                    Log.v(TAG, data.toString());
                    JSONArray contactos = data.getJSONArray("contacto");

                    if(contactos.length()>0){
                        Log.v(TAG, "Recorremos los contactos para agregarlos");

                        int existentes = 0, creados = 0, no_creados = 0;
                        boolean existe = false;
                        _numeros = new JSONArray();
                        for(int i=0; i<contactos.length();i++){
                            // Revisamos si existe este contacto en el telefono
                            JSONObject contacto = contactos.getJSONObject(i);
                            existe = false;
                            Log.v(TAG, "El numero es: " + contacto.getString("numero"));

                            boolean _existe = ContactHelper.contactExists(getApplicationContext(), contacto.getString("numero"));

                            if(!_existe){
                                Log.v(TAG, "Intentando crear ----------------------- ");

                                if(ContactHelper.insertContact(getContentResolver(), contacto.getString("nombre"), contacto.getString("numero"))){
                                    creados++;
                                    Log.v(TAG, contacto.getString("nombre") + " con numero " + contacto.getString("numero") + " fue creado satisfactoriamente.");
                                    existe = true;
                                } else {
                                    Log.e(TAG, contacto.getString("nombre") + " con numero " + contacto.getString("numero") + " no pudo ser creado.");
                                    no_creados++;
                                    existe = false;
                                }
                            } else {
                                existentes++;
                                existe = true;
                            }

                            if(existe){
                                Log.v(TAG, "El numero " + contacto.getString("numero") + " esta en la lista de contactos, listo para enviar a whatsapp");
                                _numeros.put(contacto);
                            }
                        }

                        Log.v(TAG, "Mensaje a enviar: " + data.getString("mensaje"));
                        _mensaje = data.getString("mensaje");
                        Log.v(TAG, "Enviar a: " + _numeros.toString());


                        if(_numeros.length()>0){
                            Log.v(TAG, "Hay numeros para enviar existentes o nuevos");
                            openWhatsapp();
                            started = true;
                            AutomatonService.activated = true;
                            Log.v(TAG, "Iniciado");
                        }
                    }

                } catch (Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, res);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable error, JSONObject obj) {
                error.printStackTrace();
                Log.e(TAG,  "Error al obtener los mensajes");
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 999 && (grantResults[0] != PackageManager.PERMISSION_GRANTED || grantResults[1] != PackageManager.PERMISSION_GRANTED || grantResults[2] != PackageManager.PERMISSION_GRANTED || grantResults[3] != PackageManager.PERMISSION_GRANTED || grantResults[4] != PackageManager.PERMISSION_GRANTED)) {
            moveTaskToBack(true);
            finish();
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }
}
