package ngalax.com.automaton2.util;

import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class Rest {

    private final String url = "http://service.centrosvirtuales.com/automaton";
    private AsyncHttpClient client;
    private String mToken = null;

    public Rest(){
        client = new AsyncHttpClient();
    }

    public void getMessages(JsonHttpResponseHandler handler){
        client.get(url, null, handler);
    }

    /*
    public void doLogin(String email, String password, JsonHttpResponseHandler handler){

        RequestParams params = new RequestParams();
        params.put("email", email);
        params.put("password", password);

        client.post(url + "/auth", params, handler);
    }

    public void reserve(String token, String imei, JsonHttpResponseHandler handler){
        RequestParams params = new RequestParams();
        params.put("imei", imei);
        params.put("api_token", token);
        client.addHeader("Authorization", "Bearer " + token);
        client.post(url + "/sms/start", params, handler);
    }

    public void getSms(String token, String imei, JsonHttpResponseHandler handler){
        RequestParams params = new RequestParams();
        params.put("imei", imei);
        params.put("api_token", token);
        client.addHeader("Authorization", "Bearer " + token);
        client.get(url + "/sms/get", params, handler);
    }


    public void sendIncomingMessage(String token, String from, String message){
        RequestParams params = new RequestParams();
        params.put("from", from);
        params.put("message", message);
        params.put("api_token", token);
        client.addHeader("Authorization", "Bearer " + token);
        client.post(url + "/sms/incoming", params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int code, Header[] h, JSONObject obj){
                SmsService.COUNT_RECEIVED++;
            }

            @Override
            public void onFailure(int code, Header[] h, String responseText, Throwable t){
                Log.d("SmsService", responseText);
            }
        });
    }*/
}
